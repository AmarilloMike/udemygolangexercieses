# Challenge 144 Factorial Redux

## Base Code
```
package main

import (
	"fmt"
)

func main() {
	c := factorial(4)
	for n := range c {
		fmt.Println(n)
	}
}

func factorial(n int) chan int {
	out := make(chan int)
	go func() {
		total := 1
		for i := n; i > 0; i-- {
			total *= i
		}
		out <- total
		close(out)
	}()
	return out
}

```


## CHALLENGE #1:
* Change the code above to execute 100 factorial computations concurrently and in parallel.
* Use the "pipeline" pattern to accomplish this

## POST TO DISCUSSION:

* What realizations did you have about working with concurrent code when building your solution? eg, what insights did you have which helped you understand working with concurrency?

* Post your answer, and read other answers, here: https://goo.gl/uJa99G

## Answer
* My name is Mike. Realizations: Concurrency is tricky - it requires careful thought and planning. In the last challenge I was cautious about the benefits. Factors like number of cores and hardware play an important role in determining whether parallelism or concurrency provide significant practical performance enhancements as compared to sequential processing.  

* When presented with a problem like this, the benefits are clear and economically significant. No 'if's and's or but's' - concurrency and parallelism on modern hardware can deliver significant performance improvements which can't be ignored.  Go is offering a 'better mousetrap'.  Best to master this type of architecture or 'get left in the dust'.      