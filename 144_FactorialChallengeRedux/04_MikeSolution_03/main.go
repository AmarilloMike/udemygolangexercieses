package main

import (
	"fmt"
)

func main() {

	for c := range factorial(gen()) {
		fmt.Println("Factorial =", c)
	}
}

func gen() chan int  {
	out := make(chan int)
	go func() {
		for x:= 0; x < 10; x++ {
			for y:=3; y < 13; y++ {
				out <- y
			}
		}
		close(out)
	}()

	return out
}

func factorial(c chan int) chan int  {
	out := make(chan int)

	go func () {
		for n := range c {
			for m:= range factor(n){
				out <- m
			}
		}
		close(out)
	}()

	return(out)
}

func factor(n int ) chan int {
	out := make(chan int)
	sum := 1
	go func() {
		for i:= n; i  > 0; i-- {
			sum *= i
		}
		out <- sum
		close(out)
	}()

	return out;
}

/*
CHALLENGE #1:
-- Change the code above to execute 100 factorial computations concurrently and in parallel.
-- Use the "pipeline" pattern to accomplish this

POST TO DISCUSSION:
-- What realizations did you have about working with concurrent code when building your solution?
-- eg, what insights did you have which helped you understand working with concurrency?
-- Post your answer, and read other answers, here: https://goo.gl/uJa99G
*/
