package main

import (
	"fmt"
	"runtime"
	"math/big"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {

	for n:= range factorial(gen()) {
		fmt.Println(n)
	}

}

func gen() chan *big.Int  {
	out := make(chan *big.Int)
	go func() {
		for x:= 0; x < 10; x++{
			for y:=3; y < 13; y++ {
				out <- big.NewInt(int64(y))
			}
		}
		close(out)
	}()

	return out
}

func factorial(c chan *big.Int) chan *big.Int  {
	out := make(chan *big.Int)

	go func () {
		for n := range c {
			d := incrementor(n)
			e := factor(d)

			for n:= range e{
				out <- n
			}

		}
		close(out)
	}()

	return(out)
}

func incrementor(n *big.Int ) chan *big.Int  {
	out := make(chan *big.Int)
	zero := big.NewInt(0)
	one := big.NewInt(1)

	go func() {
		for i:= big.NewInt(0).Set(n); i.Cmp(zero) > 0; i.Sub(i,one) {
			out <- big.NewInt(0).Set(i)
		}
		close(out)

	}()

	return out;
}

func factor(c chan *big.Int) chan *big.Int {
	out := make(chan *big.Int)

	go func() {
		sum := big.NewInt(1)
		for n := range c {
			sum = sum.Mul(sum,n)
		}
		out <- sum
		close(out)
	}()

	return out
}