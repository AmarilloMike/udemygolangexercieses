# Exercise 81-3 Variadic Greatest Number
* Write a function with one variadic parameter that finds the greatest number in a list of numbers.