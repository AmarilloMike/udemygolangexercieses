package main

import "fmt"

func greatestNum(data ...int) int {

	var gn int

	for _, val := range data {
		if val > gn {
			gn = val
		}
	}

	return gn
}

func main() {
	nums := []int{1, 99, 2, 8, 57, 36, 24}

	fmt.Println("The greatest number is: ", greatestNum(nums...))
}
