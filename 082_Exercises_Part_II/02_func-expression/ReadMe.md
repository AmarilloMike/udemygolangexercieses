# Exercise 81-2 func-expression
* Modify the previous program to use a func expression.

* Previous Exercise 81-2:

```
Write a function which takes an integer. The function will have two returns. The first return should be the argument divided by 2. The second return should be a bool that let’s us know whether or not the argument was even. For example: 
a.	half(1) returns (0, false)
b.	half(2) returns (1, true)
```

