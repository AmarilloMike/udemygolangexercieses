package main

import "fmt"

func foo(data ...int) int {

	var total int

	for _, val := range data {
		total += val
	}

	return total
}

func main() {

	fmt.Println("Total 1 ", foo(1, 2))
	fmt.Println("Total 2 ", foo(1, 2, 3))
	aSlice := []int{1, 2, 3, 4}
	fmt.Println("Total 3 ", foo(aSlice...))
	fmt.Println("Total 4 ", foo())

}
