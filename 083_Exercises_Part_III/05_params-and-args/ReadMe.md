# Exercise 81-05 Parameters and Arguments

* Write a function, foo, which can be called in all of these ways:

    ```
    func main() {
        foo(1, 2)
        foo(1, 2, 3)
        aSlice := []int{1, 2, 3, 4}
        foo(aSlice...)
        foo()
    }
    ```
