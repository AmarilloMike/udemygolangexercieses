# Exercise 81-06 projecteuler.net problem
* Find a problem at projecteuler.net then create the solution. Add a comment beneath your solution that includes a description of the problem. Upload your solution to github. Tweet me a link to your solution. 

* Send Tweet To Todd McLeod

*	Project Euler
    *	https://projecteuler.net/
    
## Selection: Problem 5

* Source: https://projecteuler.net/problem=5

* Title: Smallest multiple - Problem 5

* Description

```
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder. What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 ```
 
    