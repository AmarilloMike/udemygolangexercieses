package main

import "fmt"

func main() {

	fmt.Println("Testing (true && false) || (false && true) || !(false && false)")

	fmt.Println("Result is : ", (true && false) || (false && true) || !(false && false))
}
