package main

import (
	"fmt"
	"sort"
)

func main() {
	s := []string{"Zeno", "John", "Al", "Jenny"}
	fmt.Println("s before sort:", s)
	sort.Strings(s)
	fmt.Println("s after sort:", s)
}
