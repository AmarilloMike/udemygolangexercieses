package main

import (
	"fmt"
	"sort"
)

// SortPeople Exported interface
type SortPeople interface {
	sortPeople() []string
}

// Names Exported Type
type Names []string

func (n Names) sortPeople() []string {
	sort.Strings(n)
	return n
}

func main() {
	n := Names{"Zeno", "John", "Al", "Jenny"}
	fmt.Println("Names before sort:", n)
	printSortedNames(n)
}

func printSortedNames(s SortPeople) {
	fmt.Println("Sorted People: ", s.sortPeople())
}
