package main

import (
	"fmt"
	"sort"
)

// SortedNumbers Exported Interface
type SortedNumbers interface {
	SortNums() []int
}

// Numbers Exported Type
type Numbers []int

// SortNums Exported Method
func (n Numbers) SortNums() []int {
	sort.Ints(n)
	return n
}

func main() {
	n := Numbers{7, 4, 8, 2, 9, 19, 12, 32, 3}
	fmt.Println("Unsorted Numbers:", n)
	printSortedNumbers(n)
}

func printSortedNumbers(sn SortedNumbers) {
	fmt.Println("  Sorted Numbers:", sn.SortNums())
}
