package main

import (
	"fmt"
	"sort"
)

// SortPeople Exported Interface
type SortPeople interface {
	sortPeople() []string
}

// StudyGroup Exported Structure
type StudyGroup struct {
	people [] string
}

func (s StudyGroup) sortPeople() [] string {
	sort.Strings(s.people)
	return s.people
}

func main() {
	s := StudyGroup{
		people:	[]string{"Zeno", "John", "Al", "Jenny"},
	}

	fmt.Println("Unsorted Study Group:", s.people)

	printPeople(s)

}

func printPeople( sp SortPeople){
	fmt.Println("Sorted People ",sp.sortPeople())
}