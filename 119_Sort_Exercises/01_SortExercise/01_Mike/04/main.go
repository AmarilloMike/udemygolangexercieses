package main

// Exercise Solution

import (
	"fmt"
	"sort"
)

type people []string

func (p people) Len() int           { return len(p) }
func (p people) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p people) Less(i, j int) bool { return p[i] < p[j] }

func main() {
	studyGroup := people{"Zeno", "John", "Al", "Jenny"}

	fmt.Println("before sort 1     :", studyGroup)
	sort.Sort(studyGroup)
	fmt.Println("after sort 1      :", studyGroup)
	sort.Sort(sort.Reverse(studyGroup))
	fmt.Println("after reverse sort:", studyGroup)

}

// https://golang.org/pkg/sort/#Sort
// https://golang.org/pkg/sort/#Interface
