package main

import (
	"fmt"
	"sort"
)

func main() {
	n := []int{7, 4, 8, 2, 9, 19, 12, 32, 3}
	fmt.Println("----------------- Revese Sort -----------------")
	fmt.Println("n before sort:", n)
	sort.Sort(sort.Reverse(sort.IntSlice(n)))
	fmt.Println("n after sort :", n)
}
