package main

import (
	"fmt"
	"sort"
)

// SortedNumbers Exported Interface
type SortedNumbers interface {
	SortNums() []int
	ReverseSort() []int
}

// Numbers Exported Type
type Numbers []int

// SortNums Exported Method
func (n Numbers) SortNums() []int {
	sort.Ints(n)
	return n
}

// ReverseSort Exported Method
func (n Numbers) ReverseSort() []int {
	n.SortNums()
	sort.Sort(sort.Reverse(sort.IntSlice(n)))
	return n
}

func main() {
	n := Numbers{7, 4, 8, 2, 9, 19, 12, 32, 3}
	fmt.Println("Unsorted Numbers:", n)
	printReverseSortedNumbers(n)
}

func printReverseSortedNumbers(sn SortedNumbers) {
	fmt.Println("  Reverse Sort  :", sn.ReverseSort())
}
