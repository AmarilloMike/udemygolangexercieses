package main

import (
	"fmt"
)

func half(n int) (int, bool) {

	return n / 2, n%2 == 0
	// expression n%2 == 0 evaluates to a bool
}

func main() {

	fmt.Println(half(5))
}
