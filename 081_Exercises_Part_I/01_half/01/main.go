package main

import (
	"fmt"
)

func half(n int) (vhalf int, isEven bool) {
	vhalf = n / 2
	isEven = (n%2 == 0)
	return
}

func main() {

	nums := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	for _, v := range nums {

		fmt.Printf("Input %d half() output: ", v)
		fmt.Println(half(v))
	}

}
