package main

import (
	"fmt"
)

func main() {
	f := factorial(14)
	fmt.Println("Total:", f)
}

func factorial(n int64) int64 {
	total := int64(1)
	for i := n; i > 0; i-- {
		total *= i
	}
	return total
}

