# Factorial Challenge
* Example of a straight factorial calculation in 'go' 
    ```
    package main
    
    import (
        "fmt"
    )
    
    func main() {
        f := factorial(4)
        fmt.Println("Total:", f)
    }
    
    func factorial(n int) int {
        total := 1
        for i := n; i > 0; i-- {
            total *= i
        }
        return total
    }
    ```

/*
CHALLENGE #1:
-- Use goroutines and channels to calculate factorial

CHALLENGE #2:
-- Why might you want to use goroutines and channels to calculate factorial?
---- Formulate your own answer, then post that answer to this discussion area: https://goo.gl/flGsyX
---- Read a few of the other answers at the discussion area to see the reasons of others
*/

## CHALLENGE #2:
-- Why might you want to use goroutines and channels to calculate factorial?

*   ##### Answer
 Using goroutines and channels in calculating large factorials or large numbers of factorials may improve performance. The performance benefit may be more relevant when executing complex factorial calculations associated with 'combinations' and 'permutations'. Some of these formulas require multiple factorial computations which could be executed in parallel using goroutines and channels.  
     