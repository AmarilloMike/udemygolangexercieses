package main

import "fmt"

func main() {
	var myName string
	fmt.Print("Please enter your name: ")
	fmt.Scan(&myName)
	// This works too!
	//fmt.Printf("Hello %s\n", myName )
	fmt.Println("Hello", myName)
}
