package main

import "fmt"

func main() {

	fmt.Println("Printing all even numbers between 0 and 100: \n")

	for i := 0; i <= 100; i++ {
		if i%2 == 0 {
			fmt.Println(i)
		}
	}

	fmt.Println("")
}
