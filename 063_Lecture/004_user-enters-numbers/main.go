package main

import "fmt"

func main() {

	var largeNum int64
	fmt.Print("Please enter a large number: ")
	fmt.Scan(&largeNum)

	var smallNum int64
	fmt.Print("Please enter a small number: ")
	fmt.Scan(&smallNum)

	fmt.Printf("\nThe remainder of the large number, %d\n", largeNum)
	fmt.Printf("divided by the small number, %d, is: %d\n", smallNum, largeNum%smallNum)

}
