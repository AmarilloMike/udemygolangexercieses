package main

import "fmt"

func main() {

	fmt.Println("Printing numbers 1 to 100")
	fmt.Println("Multiples of 3 = 'Fizz'")
	fmt.Println("Multiples of 5 = 'Buzz'")
	fmt.Println("Multiples of 3 and 5 = 'FizzBuzz'\n\n")

	for i := 0; i <= 100; i++ {

		if i%3 == 0 && i%5 == 0 {

			fmt.Println("FizzBuzz")

		} else if i%5 == 0 {
			fmt.Println("Buzz")
		} else if i%3 == 0 {
			fmt.Println("Fizz")
		} else {
			fmt.Println(i)
		}

	}
}
