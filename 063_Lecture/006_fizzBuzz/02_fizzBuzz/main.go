package main

import (
		"fmt"
		"strconv"
)

func main() {
	fmt.Println("Printing numbers 1 to 100")
	fmt.Println("Multiples of 3 = 'Fizz'")
	fmt.Println("Multiples of 5 = 'Buzz'")
	fmt.Println("Multiples of 3 and 5 = 'FizzBuzz'\n\n")

	for i := 0; i <= 100; i++ {

		outStr := ""

		if i%3 == 0 {
			outStr = "Fizz"
		}

		if i%5 == 0 {
			outStr += "Buzz"
		}

		if outStr == "" {
			outStr =  strconv.Itoa(i)
		}

		fmt.Println(outStr)
	}
}
