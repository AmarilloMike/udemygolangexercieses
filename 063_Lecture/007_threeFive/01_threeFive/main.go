package main

import "fmt"

func main() {
	fmt.Println("Finding the sum of all the natural numbers")
	fmt.Println("between 1 and 1000 which are multiples of 3 or 5.\n")

	sum := 0
	cnt := 0

	for i := 0; i < 1000; i++ {
		if i%3 == 0 || i%5 == 0 {
			cnt++
			sum += i
			println(i)
		}
	}

	fmt.Println("\n------------------------")
	fmt.Println("Total includes ", cnt, " numbers")
	fmt.Println("The total is:", sum)
}
