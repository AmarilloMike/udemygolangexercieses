package main

import (
	"fmt"
)

func main() {

	adds:= genAdds()
	sum:= 0
	for _, c := range adds{
		for n := range c {
			sum += n
		}
	}

	fmt.Println("Total ", sum)
}

func genAdds() [] chan int  {

	var adds [] chan int

	doAdd := func() chan int {

		out := make(chan int)
		go func(){
			for i:= 0; i < 20; i++ {
				out <- 1
			}
			close(out)
		}()

		return out
	}

	adds = append(adds,doAdd())
	adds = append(adds,doAdd())
	return adds
}
