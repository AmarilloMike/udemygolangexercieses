package main

import (
	"fmt"
	"runtime"
	"time"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

// Increased the number of loops from 4 to 10
func main() {
	t1 := time.Now()

	var outputs = []chan int{}

	for i := 0; i < 10; i++ {
		in := gen()
		out := factorial(in)
		outputs = append(outputs, out)
	}

	cnt := 0

	for c := range mergeOutputs(outputs) {
		cnt++
		fmt.Println("Count", cnt, " Factorial ", c)
	}
	t2 := time.Now()
	fmt.Println("Elapsed Time:", t2.Sub(t1))
}

func mergeOutputs(o []chan int) chan int {
	out := make(chan int)

	go func() {
		for _, n := range o {
			for c := range n {
				out <- c
			}
		}
		close(out)
	}()

	return out
}

func gen() chan int {
	c := make(chan int)
	go func(d chan int) {

		for i := 0; i < 10; i++ {
			for j := 3; j < 13; j++ {
				d <- j
			}
		}

		close(d)

	}(c)

	return c
}

func factorial(in chan int) chan int {

	out := make(chan int)

	go func(d chan int) {
		for n := range in {
			d <- fact(n)
		}

		close(d)

	}(out)

	return out
}

func fact(n int) int {
	total := 1
	for i := n; i > 0; i-- {
		total *= i
	}
	return total
}
