package main

import (
	"fmt"
	"runtime"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {

	var outputs = []chan int{}

	for i := 0; i < 4; i++ {
		in := make(chan int)
		go gen(in)
		out := make(chan int)
		go factorial(in, out)
		outputs = append(outputs, out)
	}

	cnt := 0

	for _, n := range outputs {
		for c := range n {
			cnt++
			fmt.Println("Count", cnt, " Factorial ", c)
		}
	}
}

func gen(c chan<- int) {

	for i := 0; i < 25; i++ {
		for j := 3; j < 13; j++ {
			c <- j
		}
	}

	close(c)
}

func factorial(in <-chan int, out chan<- int) {

	for n := range in {
		out <- fact(n)
	}

	close(out)
}

func fact(n int) int {
	total := 1
	for i := n; i > 0; i-- {
		total *= i
	}
	return total
}
