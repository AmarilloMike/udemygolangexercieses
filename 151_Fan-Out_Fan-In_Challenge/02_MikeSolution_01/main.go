package main

import (
	"fmt"
	"runtime"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}


func main() {


	nums := make(chan int)
	out := make(chan int)
	for i := 0; i < 4; i++ {
		go gen(nums)
		go factorial(nums, out)
	}

	cnt := 0
	for c := range out {
		cnt++
		fmt.Println("Count ", cnt, "Factorial=", c)
		if(cnt==1000){
			close(nums)
			close(out)
		}
	}

	fmt.Println("Finished")

}

func gen(c chan <- int) {
	for i := 0; i < 25; i++ {
		for j := 3; j < 13; j++ {
			c <- j
		}
	}
}

func factorial(in <-chan int, out chan <- int ) {

	for n := range in {
		out <- fact(n)
	}

}

func fact(n int) int {
	total := 1
	for i := n; i > 0; i-- {
		total *= i
	}
	return total
}
