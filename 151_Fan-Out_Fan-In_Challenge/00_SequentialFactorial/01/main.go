package main

import (
	"fmt"
	"time"
)

func main() {

	t1 := time.Now();

	in := gen()
	cnt := 0
	for i:= 0; i < len(in); i++ {
		cnt++
		fmt.Println(fact(in[i]))
	}

	t2:= time.Now();

	fmt.Println(" Count:", cnt ," Elapsed Time:", t2.Sub(t1))
}

func gen() [] int {
	out := make([] int, 1000, 1000)
	x := 0
	for i := 0; i < 100; i++ {
		for j := 3; j < 13; j++ {
			out[x] = j
			x++
		}
	}

	return out
}

func fact(n int) int {
	total := 1
	for i := n; i > 0; i-- {
		total *= i
	}
	return total
}

// Sample of 5 run times. Average = 57.79662 ms


/*
CHALLENGE #1:
-- Change the code above to execute 1,000 factorial computations concurrently and in parallel.
-- Use the "fan out / fan in" pattern

CHALLENGE #2:
WATCH MY SOLUTION BEFORE DOING THIS CHALLENGE #2
-- While running the factorial computations, try to find how much of your resources are being used.
-- Post the percentage of your resources being used to this discussion: https://goo.gl/BxKnOL
*/
