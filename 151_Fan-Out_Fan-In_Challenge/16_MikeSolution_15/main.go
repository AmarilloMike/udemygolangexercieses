package main

import (
	"fmt"
	"math/big"
	"sync"
	"time"
	"runtime"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}


// Running 10,000 factorials
// Using MAX Processors
// Using different form of merge
// Using big.Int to provide for large Factorial numbers
func main() {
	t1 := time.Now()

	var outputs = []chan *big.Int{}
	in := make(chan *big.Int)

	gen(in)

	for i := 0; i < 10; i++ {
		out := factorial(in)
		outputs = append(outputs, out)
	}

	cnt := big.NewInt(0)
	one := big.NewInt(1)

	for c := range mergeOutputs(outputs) {
		cnt = cnt.Add(cnt, one)
		fmt.Println(cnt, "\t", c)
	}
	t2 := time.Now()
	fmt.Println("Elapsed Time:", t2.Sub(t1))
}

func mergeOutputs(o []chan *big.Int) chan *big.Int {
	var wg sync.WaitGroup
	out := make(chan *big.Int)

	output := func(c <-chan *big.Int) {

		for n := range c {
			out <- n
		}
		wg.Done()
	}

	wg.Add(len(o))

	for _, c := range o {
		go output(c)
	}

	// Start a goroutine to close out once all the output goroutines are
	// done.  This must start after the wg.Add call.
	go func() {
		wg.Wait()
		close(out)
	}()

	return out

}

func gen(c chan *big.Int) {
	var wg sync.WaitGroup

	one := big.NewInt(1)

	genOutput := func(d chan *big.Int) {

		limit_10 := big.NewInt(10)
		limit_13 := big.NewInt(13)

		for i := big.NewInt(0); i.Cmp(limit_10) < 0; i.Add(i,one) {
			for j := big.NewInt(3); j.Cmp(limit_13) < 0; j.Add(j,one) {
				d <- big.NewInt(0).Set(j)
			}
		}
		wg.Done()
	}

	const cycles = 100

	wg.Add(cycles)
	cyclesLimit := big.NewInt(int64(cycles))

	for i := big.NewInt(0); i.Cmp(cyclesLimit) < 0; i.Add(i,one) {
		go genOutput(c)
	}

	go func() {
		wg.Wait()
		close(c)
	}()

}

func factorial(in chan *big.Int) chan *big.Int {

	out := make(chan *big.Int)

	go func(d chan *big.Int) {
		for n := range in {
			d <- fact(n)
		}

		close(d)

	}(out)

	return out
}

func fact(n *big.Int) *big.Int {
	zero := big.NewInt(0)
	one := big.NewInt(1)
	total := big.NewInt(1)

	for i:= big.NewInt(0).Set(n); i.Cmp(zero) > 0; i.Sub(i,one) {

		total = total.Mul(total, big.NewInt(0).Set(i))
	}
	return total
}
