package main

import (
	"fmt"
	"sync"
	"time"
)


// Running 1,000 factorials
// NOT using MAX Processors
func main() {
	t1 := time.Now()

	var outputs = []chan int{}
	in := make(chan int)

	gen(in)

	for i := 0; i < 10; i++ {
		out := factorial(in)
		outputs = append(outputs, out)
	}

	cnt := 0

	for c := range mergeOutputs(outputs) {
		cnt++
		fmt.Println(cnt, "\t", c)
	}
	t2 := time.Now()
	fmt.Println("Elapsed Time:", t2.Sub(t1))
}

func mergeOutputs(o []chan int) chan int {
	var wg sync.WaitGroup
	out := make(chan int)

	output := func(c <-chan int) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}

	wg.Add(len(o))

	for _, c := range o {
		go output(c)
	}

	// Start a goroutine to close out once all the output goroutines are
	// done.  This must start after the wg.Add call.
	go func() {
		wg.Wait()
		close(out)
	}()

	return out

}

func gen(c chan int) {
	var wg sync.WaitGroup

	genOutput := func(d chan int) {
		for i := 0; i < 10; i++ {
			for j := 3; j < 13; j++ {
				d <- j
			}
		}
		wg.Done()
	}

	wg.Add(10)

	for i := 0; i < 10; i++ {
		go genOutput(c)
	}

	go func() {
		wg.Wait()
		close(c)
	}()

}

func factorial(in chan int) chan int {

	out := make(chan int)

	go func(d chan int) {
		for n := range in {
			d <- fact(n)
		}

		close(d)

	}(out)

	return out
}

func fact(n int) int {
	total := 1
	for i := n; i > 0; i-- {
		total *= i
	}
	return total
}
