package main

import (
	"fmt"
	"runtime"
	"time"
	"sync"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

// Running 5,000 factorials
func main() {
	t1 := time.Now()

	var outputs = []chan int{}

	in := gen()

	for i := 0; i < 10; i++ {
		out := factorial(in)
		outputs = append(outputs, out)
	}

	cnt := 0

	for c := range mergeOutputs(outputs) {
		cnt++
		fmt.Println("Count", cnt, " Factorial ", c)
	}
	t2 := time.Now()
	fmt.Println("Elapsed Time:", t2.Sub(t1))
}

func mergeOutputs(o []chan int) chan int {
	var wg sync.WaitGroup
	out := make(chan int)

	output := func(c <-chan int) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}

	wg.Add(len(o))

	for _, c := range o {
		go output(c)
	}

	// Start a goroutine to close out once all the output goroutines are
	// done.  This must start after the wg.Add call.
	go func() {
		wg.Wait()
		close(out)
	}()

	return out

}

func gen() chan int {
	var wg sync.WaitGroup

	c := make(chan int)
	genOutput := func(d chan int) {
		for i := 0; i < 100; i++ {
			for j := 3; j < 13; j++ {
				d <- j
			}
		}
		wg.Done()
	}

	wg.Add(5)

	for i:=0; i < 5; i++ {
		go genOutput(c)
	}

	go func() {
		wg.Wait()
		close(c)
	}()

	return c
}

func factorial(in chan int) chan int {

	out := make(chan int)

	go func(d chan int) {
		for n := range in {
			d <- fact(n)
		}

		close(d)

	}(out)

	return out
}

func fact(n int) int {
	total := 1
	for i := n; i > 0; i-- {
		total *= i
	}
	return total
}
